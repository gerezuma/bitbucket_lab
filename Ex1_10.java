package LabNotGraded;

public class Ex1_10 {

	public static void main(String[] args) {
		double kilometers = 14;
		double minutes = 45;
		double seconds = 30;
		
		hoursConversion(minutes, seconds, kilometers);

	}
	public static double hoursConversion(double minutes, double seconds, double kilometers) {
	if((seconds < 0) && (minutes == 0)) {
		return 0;
	}else {
		double milesPerHour = kilometers/ (((minutes/60) + (seconds/3600)) * 1.6);
		System.out.println("The average speed of a person running " + kilometers + 
				" kilometers \nin "+ minutes + " and " + seconds + " seconds, \nconverted to miles per hour is " 
				+ String.format("%.2f",milesPerHour) + " miles per hour!");
		return milesPerHour;
	}
}
}
